#include "DiscreteLogarithmSignature.h"


DiscreteLogarithmSignature::DiscreteLogarithmSignature(){}

void DiscreteLogarithmSignature::init(std::string& M, std::string state, std::string other_information) {
	if (state == "encoding")
		makeSignature(M, other_information);
	else if (state == "decoding")
		extractSignature(M);
	else {
		std::cout << "Wrong signature state!\n";
		system("pause");
	}
}

void DiscreteLogarithmSignature::sign(std::string& M) {
	M += toString(p);
	M += toString(q);
	M += toString(g);
	M += toString(y);
	M += toString(r);
	M += toString(s);
}

bool DiscreteLogarithmSignature::checkSignature(std::string M) {
	uint256_t h = makeHash(M),
		ro = r % q;

	uint256_t u1 = uint256_t((uint512_t(pow(h, q - 2, q)) * s) % q),
		u2 = uint256_t((uint512_t(pow(h, q - 2, q)) * ro) % q);
	uint256_t r0 = uint256_t((uint512_t((pow(g, u1, p)) * uint512_t(pow(y, u2, p))) % p) % q);
	if (r0 != r)
		return false;

	return true;
}

void DiscreteLogarithmSignature::makeSignature(std::string& M, std::string other_information) {
	information = "\n!!!SIGNATURE STARTS!!!\nSigning date: " + boost::gregorian::to_iso_extended_string(boost::gregorian::day_clock::local_day()) +
		". Expiring date: " + boost::gregorian::to_iso_extended_string(boost::gregorian::day_clock::local_day() + boost::gregorian::date_duration(1)) +
		". Signed by Savelyev Nikita. " + other_information + "\n!!!SIGNATURE ENDS!!!\n";
	M += information;

	std::ifstream in(primes_file);
	in >> p >> q;
	in.close();

	if ((p - 1) % q != 0) {
		std::cout << "p % q != 0\n";
		system("pause");
	}

	uint256_t gamma, x, h, ro, k;
	gamma = getRandomValue(2, p - 2);
	x = getRandomValue(2, q - 1);
	k = getRandomValue(1, q - 1);

	g = pow(gamma, (p - 1) / q, p);
	y = pow(g, x, p);
	h = makeHash(M);

	r = pow(g, k, p) % q;
	ro = r % q;
	s = ((uint256_t((uint512_t(h)*k) % q) + q) - uint256_t((uint512_t(ro)*x) % q)) % q;

	if (uint256_t((uint512_t(h)*k) % q) != (s + uint256_t((uint512_t(ro)*x) % q)) % q) {
		std::cout << "Making signature failure\n";
		system("pause");
	}
	else
		std::cout << "Signature created successfully\n";
}

void DiscreteLogarithmSignature::extractSignature(std::string& C) {
	s = toUint256(C.substr(C.size() - 32, 32));
	C.erase(C.size() - 32, 32);

	r = toUint256(C.substr(C.size() - 32, 32));
	C.erase(C.size() - 32, 32);

	y = toUint256(C.substr(C.size() - 32, 32));
	C.erase(C.size() - 32, 32);

	g = toUint256(C.substr(C.size() - 32, 32));
	C.erase(C.size() - 32, 32);

	q = toUint256(C.substr(C.size() - 32, 32));
	C.erase(C.size() - 32, 32);

	p = toUint256(C.substr(C.size() - 32, 32));
	C.erase(C.size() - 32, 32);
}

void DiscreteLogarithmSignature::show() {
	std::cout << p << std::endl <<
		q << std::endl <<
		g << std::endl <<
		y << std::endl <<
		r << std::endl <<
		s << std::endl;
}