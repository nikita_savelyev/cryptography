#include "Other.h"


Timer::Timer(){}

void Timer::start() {
	time_stack.push(clock());
}

double Timer::stop() {
	double result;

	result = double(clock() - time_stack.top()) / CLOCKS_PER_SEC;
	time_stack.pop();

	return result;
}


int getByte(uint128_t arg, int pos) {
	uint128_t result;

	uint128_t bit_mask = 255;
	bit_mask <<= 8 * (15 - pos);
	result = arg & bit_mask;
	result >>= 8 * (15 - pos);

	return int(result);
}

int getByte(uint256_t arg, int pos) {
	uint256_t result;

	uint256_t bit_mask = 255;
	bit_mask <<= 8 * (31 - pos);
	result = arg & bit_mask;
	result >>= 8 * (31 - pos);

	return int(result);
}

uint128_t getByteSegment(uint128_t arg, int left, int right) {
	uint128_t result = 0;

	for (int i = left; i <= right; i++) {
		result <<= 8;
		result = result + getByte(arg, i);
	}

	return result;
}

uint128_t connect8ByteSegments(uint128_t arg1, uint128_t arg2) {
	uint128_t result;

	result = arg1;
	result <<= 64;
	result = result + arg2;

	return result;
}

uint128_t toUint128(std::string arg) {
	uint128_t result = 0;

	for (int i = 0; i < 16; i++) {
		result <<= 8;
		result = result + int(unsigned char(arg[i]));
	}

	return result;
}

uint256_t toUint256(std::string arg) {
	uint256_t result = 0;

	for (int i = 0; i < 32; i++) {
		result <<= 8;
		result = result + int(unsigned char(arg[i]));
	}

	return result;
}

std::string toString(uint128_t arg) {
	std::string result;

	for (int i = 0; i < 16; i++)
		result.push_back(char(getByte(arg, i)));

	return result;
}

std::string toString(uint256_t arg) {
	std::string result;

	for (int i = 0; i < 32; i++)
		result.push_back(char(getByte(arg, i)));

	return result;
}

uint256_t getRandomValue(uint256_t left_boundary, uint256_t right_boundary) {
	uint256_t result = 0;

	std::random_device rd;
	for (int i = 0; i < 4; i++) {
		result <<= 64;
		result += rd();
	}
	if (right_boundary != -1)
		result = (result % (right_boundary - left_boundary + 1)) + left_boundary;
	else
		if (result < left_boundary)
			result += left_boundary;

	return result;
}

uint256_t GCD(uint256_t arg1, uint256_t arg2) {
	uint256_t result;

	uint256_t temp;
	while (arg1 != 0) {
		temp = arg1;
		arg1 = arg2 % arg1;
		arg2 = temp;
	}
	result = arg2;

	return result;
}

uint256_t pow(uint256_t arg1, uint256_t arg2, uint256_t mod) {
	uint256_t result = 1;

	while (arg2 > 0) {
		if (arg2 % 2 == 1)
			result = uint256_t((uint512_t(result) * arg1) % mod);
		arg1 = uint256_t((uint512_t(arg1) * arg1) % mod);
		arg2 /= 2;
	}

	return result;
}

bool isPrime(uint256_t n, int number_of_tests) {
	//std::cout << n << std::endl;
	if (n < 2)
		return false;
	else if (n == 2)
		return true;

	bool result = true;

	int s = 0;
	uint256_t d = n - 1;
	while (d % 2 == 0) {
		s++;
		d /= 2;
	}

	for (int i = 0; i < number_of_tests && result; i++) {
		uint256_t a = getRandomValue(2, n - 1);

		if (GCD(n, a) > 1)
			result = false;
		else {
			if (pow(a, d, n) != 1) {
				bool e = false;
				for (int r = 0; r < s && !e; r++) {
					if (pow(a, pow(2, r, n)*d, n) == n - 1)
						e = true;
				}
				if (!e)
					result = false;
			}
		}
	}

	return result;
}


uint128_t getBigPrime() {
	uint128_t result;

	result = uint128_t(getRandomValue());
	if (result % 2 == 0)
		result++;
	while (!isPrime(result))
		result += 2;

	return result;
}

uint128_t getMaxFactor(uint128_t n) {
	if (isPrime(n) || n == 1)
		return n;
	if (n == 4)
		return 2;

	uint128_t result = 1;

	uint256_t x = getRandomValue(1, n - 2),
		y = 1,
		stage = 2,
		gcd = GCD(n, abs(x - y));
	int i = 0;
	while (gcd == 1) {
		if (i == stage) {
			y = x;
			stage *= 2;
		}
		x = (x * x + 1) % n;
		i++;

		gcd = GCD(n, abs(x - y));
	}
	uint128_t res1, res2;
	res1 = getMaxFactor(uint128_t(gcd));
	res2 = getMaxFactor(n / uint128_t(gcd));
	if (res1 > res2)
		result = res1;
	else
		result = res2;

	return result;
}

uint256_t getBiggerPrime(uint128_t q) {
	uint256_t result;

	if (!isPrime(q)) {
		system("pause");
		return q;
	}

	
	result = (uint256_t(1) << 128) * q + 1;
	while (!isPrime(result))
		result += 2 * q;

	return result;
}

void printPairOfPrimes() {
	std::ofstream primes_txt("primes.txt");

	uint128_t q = getBigPrime();
	uint256_t p = getBiggerPrime(q);

	primes_txt << p << std::endl << q << std::endl;
}