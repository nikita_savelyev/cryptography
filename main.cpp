#include "Magenta.h"
using namespace std;

int main() {
	//printPairOfPrimes();

	int kmode;
	cout << "new random key? 1 - yes, 0 - no\n";
	cin >> kmode;

	uint256_t K;
	if (kmode) {
		K = getRandomValue();
		ofstream key_out("key.txt");
		key_out << K << endl;
		key_out.close();
	}
	else {
		ifstream key_in("key.txt");
		key_in >> K;
		key_in.close();
	}
	Magenta code(K, "DL");

	int codemode;
	cout << "encode or decode? 2 - both, 1 - encode, 0 - decode\n";
	cin >> codemode;
	if (codemode == 1) {
		ifstream to_code("input.txt");
		ofstream coded("code.txt");
		code.encode(to_code, coded);
		to_code.close();
		coded.close();
	}
	else if (codemode == 0) {
		ifstream to_decode("code.txt");
		ofstream decoded("output.txt");
		code.decode(to_decode, decoded);
		to_decode.close();
		decoded.close();
	}
	else {
		ifstream to_code("input.txt");
		ofstream coded("code.txt");
		code.encode(to_code, coded);
		to_code.close();
		coded.close();
		ifstream to_decode("code.txt");
		ofstream decoded("output.txt");
		code.decode(to_decode, decoded);
		to_decode.close();
		decoded.close();
	}
	system("pause");
	return 0;
}