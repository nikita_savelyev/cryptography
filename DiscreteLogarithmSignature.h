#ifndef _D_LSIGN_
#define _D_LSIGN_

#include "Hash.h"
#include "Signature.h"
#include "boost/date_time/gregorian/gregorian.hpp"
const std::string primes_file = "primes.txt";
using namespace boost::multiprecision;


class DiscreteLogarithmSignature : public Signature {
private:
	uint256_t p;
	uint256_t q;
	uint256_t g;
	uint256_t y;
	uint256_t r;
	uint256_t s;
	std::string information;

	void makeSignature(std::string& M, std::string other_information);
	void extractSignature(std::string& C);

public:
	DiscreteLogarithmSignature();
	void init(std::string& M, std::string state, std::string other_information = "");
	void sign(std::string& M);
	bool checkSignature(std::string M);
	void show();
};


#endif