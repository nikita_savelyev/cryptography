#include "Hash.h"

const uint128_t H0 = 100500;

uint128_t makeHash(std::string M) {
	uint128_t result = H0;

	while (M.length() % 16 != 0)
		M += ' ';

	for (size_t i = 0; i < M.length() / 16; i++) {
		std::string str = M.substr(i, 16);

		uint128_t block = toUint128(str);
		Magenta code(uint256_t(block), "DL");
		result = code.encode(block ^ result) ^ block ^ result;
	}

	return result;
}