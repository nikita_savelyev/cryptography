#include "GF.h"



Polynom::Polynom(){
	for (int i = 0; i < POLYNOM_POWER; i++)
		coefficients[i] = 0;
}

Polynom::Polynom(double _coefficients[POLYNOM_POWER]) {
	for (int i = 0; i < POLYNOM_POWER; i++)
		coefficients[i] = _coefficients[i];
}

Polynom::Polynom(const double _coefficients[POLYNOM_POWER]) {
	for (int i = 0; i < POLYNOM_POWER; i++)
		coefficients[i] = _coefficients[i];
}

int Polynom::getMaxPower() const {
	int result = -1;

	for (int i = POLYNOM_POWER - 1; i >= 0 && result == -1; i--)
		if (abs(coefficients[i]) > EPS)
			result = i;

	return result;
}

void Polynom::show() {
	for (int i = 0; i < POLYNOM_POWER; i++)
		cout << int(coefficients[i]) << " ";
	cout << endl;
}

Polynom Polynom::operator = (const Polynom& other) {
	if (this != &other) {
		for (int i = 0; i < POLYNOM_POWER; i++)
			coefficients[i] = other.coefficients[i];
	}

	return *this;
}



Polynom operator + (const Polynom& first, const Polynom& second) {
	Polynom result;

	for (int i = 0; i < POLYNOM_POWER; i++)
		result.coefficients[i] = first.coefficients[i] + second.coefficients[i];

	return result;
}

Polynom operator - (const Polynom& first, const Polynom& second) {
	return first + -second;
}

Polynom operator * (const Polynom& first, const Polynom& second) {
	Polynom result;

	for (int i = 0; i < POLYNOM_POWER; i++)
		for (int j = 0; j < POLYNOM_POWER; j++)
			result.coefficients[i + j] += first.coefficients[i] * second.coefficients[j];

	return result;
}

Polynom operator % (const Polynom& first, const Polynom& second) {
	Polynom result;
	
	if (first.getMaxPower() < second.getMaxPower())
		result = first;
	else {
		Polynom temp;
		temp.coefficients[first.getMaxPower() - second.getMaxPower()] = first.coefficients[first.getMaxPower()] / second.coefficients[second.getMaxPower()];

		temp = temp * second;
		temp = first - temp;
		
		result = temp % second;
	}

	for (int i = 0; i < POLYNOM_POWER; i++)			//for field
		result.coefficients[i] = abs(result.coefficients[i]);

	return result;
}


int getFieldElem(int x) {
	if (x == 0)
		return 1;
	else
		x--;
	Polynom polynom = generating_polynom;

	for (int i = 0; i < x; i++)
		polynom = (polynom * generating_polynom) % defualt_polynom;

	int result = 0, twos = 1;
	for (int i = 0; i < POLYNOM_POWER; i++) {
		result += int(polynom.coefficients[i]) * twos;
		twos *= 2;
	}
	return result;
}


Polynom operator - (const Polynom& other) {
	Polynom result;

	for (int i = 0; i < POLYNOM_POWER; i++)
		result.coefficients[i] = -other.coefficients[i];

	return result;
}