#ifndef _SIGNATURE_
#define _SIGNATURE_

#include <string>

class Signature {
public:
	virtual void sign(std::string& M) = 0;
	virtual bool checkSignature(std::string M) = 0;
	virtual void init(std::string& M, std::string state, std::string other_information = "") = 0;
	virtual void show() = 0;
};


#endif