#ifndef MAGENTA_H
#define MAGENTA_H

#include "Other.h"
#include "DiscreteLogarithmSignature.h"
#include "EllipticCurveSignature.h"
using namespace boost::multiprecision;

const uint128_t P0 = 100500;

class Magenta {
private:
	int f[256];
	char hex[16];
	int rev_hex[256];
	int key_mode;
	uint256_t key;
	uint128_t round_keys[5];
	class Signature* signature;

	int A(int arg1, int arg2);
	int PE(int arg1, int arg2);
	uint128_t P(uint128_t arg);
	uint128_t T(uint128_t arg);
	uint128_t XE(uint128_t arg);
	uint128_t XO(uint128_t arg);
	uint128_t S(uint128_t arg);
	uint128_t C(uint128_t arg, int k);
	uint128_t E(uint128_t arg);
	uint128_t V(uint128_t arg);
	uint128_t F(uint128_t arg, uint128_t round_key);
	uint128_t magenta(uint128_t arg);

public:
	Magenta(uint256_t _key, std::string signature_type);
	uint128_t encode(uint128_t M);
	uint128_t decode(uint128_t C);
	std::string encode(std::string M, std::string additional_signature_info = "");
	std::string decode(std::string C);
	void encode(std::ifstream &input, std::ofstream &output, std::string additional_signature_info = "");
	void decode(std::ifstream &input, std::ofstream &output);
};




#endif MAGENTA_H