#include "Magenta.h"

int Magenta::A(int arg1, int arg2) {
	int result;

	result = f[arg1 ^ f[arg2]];

	return result;
}

int Magenta::PE(int arg1, int arg2) {
	int result;

	result = (A(arg1, arg2) << 8) + A(arg2, arg1);

	return result;
}

uint128_t Magenta::P(uint128_t arg) {
	uint128_t result = 0;

	int temp;
	for (int i = 0; i < 8; i++) {
		temp = PE(getByte(arg, i), getByte(arg, i + 8));
		result <<= 16;
		result = result + temp;
	}

	return result;
}

uint128_t Magenta::T(uint128_t arg) {
	uint128_t result;

	result = arg;
	for (int i = 0; i < 4; i++)
		result = P(result);

	return result;
}

uint128_t Magenta::XE(uint128_t arg) {
	uint128_t result = 0;

	for (int i = 0; i < 8; i++) {
		result <<= 8;
		result = result + getByte(arg, 2 * i);
	}

	return result;
}

uint128_t Magenta::XO(uint128_t arg) {
	uint128_t result = 0;

	for (int i = 0; i < 8; i++) {
		result <<= 8;
		result = result + getByte(arg, 2 * i + 1);
	}

	return result;
}

uint128_t Magenta::S(uint128_t arg) {
	uint128_t result;

	result = connect8ByteSegments(XE(arg), XO(arg));

	return result;
}

uint128_t Magenta::C(uint128_t arg, int k) {
	uint128_t result;

	if (k == 1)
		result = T(arg);
	else
		result = T(arg ^ S(C(arg, k - 1)));

	return result;
}

uint128_t Magenta::V(uint128_t arg) {
	uint128_t result;

	uint128_t arg1, arg2;
	arg1 = getByteSegment(arg, 0, 7);
	arg2 = getByteSegment(arg, 8, 15);
	result = connect8ByteSegments(arg2, arg1);

	return result;
}

uint128_t Magenta::F(uint128_t arg, uint128_t round_key) {
	uint128_t result;

	uint128_t arg1, arg2, result1, result2;
	arg1 = getByteSegment(arg, 0, 7);
	arg2 = getByteSegment(arg, 8, 15);

	result1 = arg2;
	//result2 = XE(C(connect8ByteSegments(arg2, round_key), 3));		//wiki
	result2 = arg1 ^ XE(C(connect8ByteSegments(arg2, round_key), 3));	//pdf

	result = connect8ByteSegments(result1, result2);

	return result;
}

uint128_t Magenta::magenta(uint128_t arg) {
	uint128_t result = arg;

	if (key_mode == 16) {
		result = F(result, round_keys[4]);
		result = F(result, round_keys[4]);
		result = F(result, round_keys[3]);

		result = F(result, round_keys[3]);
		result = F(result, round_keys[4]);
		result = F(result, round_keys[4]);
	}
	else if (key_mode == 24) {
		result = F(result, round_keys[4]);
		result = F(result, round_keys[3]);
		result = F(result, round_keys[2]);

		result = F(result, round_keys[2]);
		result = F(result, round_keys[3]);
		result = F(result, round_keys[4]);
	}
	else if (key_mode == 32){
		result = F(result, round_keys[4]);
		result = F(result, round_keys[3]);
		result = F(result, round_keys[2]);
		result = F(result, round_keys[1]);

		result = F(result, round_keys[1]);
		result = F(result, round_keys[2]);
		result = F(result, round_keys[3]);
		result = F(result, round_keys[4]);
	}
	else
		exit(1);

	return result;
}


Magenta::Magenta(uint256_t _key, std::string signature_type) {
	f[0] = 1;
	for (int i = 1; i < 256; i++) {
		f[i] = (f[i - 1] << 1) % 256;

		if (f[i - 1] > 127)
			f[i] ^= 101;
	}
	f[255] = 0;



	for (char i = '0'; i <= '9'; i++) {
		hex[i - '0'] = i;
		rev_hex[i] = i - '0';
	}
	for (char i = 'a'; i <= 'f'; i++) {
		hex[i - 'a' + 10] = i;
		rev_hex[i] = i - 'a' + 10;
	}

	if (signature_type == "default") {}
	else if (signature_type == "DL")
		signature = new DiscreteLogarithmSignature();
	else {
		std::cout << "Wrong signature type!\n";
		system("pause");
	}

	key = _key;
	uint256_t bit_mask = (uint256_t(1) << 64) - 1;
	for (int i = 4; i > 0; i--) {
		round_keys[i] = uint128_t(_key & bit_mask);
		_key >>= 64;
	}
	if (round_keys[1] != 0)
		key_mode = 32;
	if (round_keys[1] == 0)
		key_mode = 24;
	if (round_keys[1] == 0 && round_keys[2] == 0)
		key_mode = 16;
}

uint128_t Magenta::encode(uint128_t M) {
	uint128_t result;

	result = magenta(M);

	return result;
}

uint128_t Magenta::decode(uint128_t C) {
	uint128_t result;

	result = V(C);
	result = encode(result);
	result = V(result);

	return result;
}

std::string Magenta::encode(std::string M, std::string additional_signature_info) {
	std::string result = "";

	std::ofstream test("test.txt");
	test << makeHash(M) << std::endl;

	Timer timer;
	timer.start();
	std::cout << "Starting encoding...\n";
	

	signature->init(M, "encoding", additional_signature_info);

	M += char(128);
	while (M.length() % 16 != 0)
		M += char(0);

	uint128_t previous_P = P0;
	for (size_t i = 0; i < M.length() / 16; i++) {
		std::string str = M.substr(16 * i, 16);

		uint128_t P = toUint128(str);
		uint128_t C = encode(P) ^ previous_P;
		previous_P = P;

		str = toString(C);
		result += str;
	}

	result += toString(P0);
	//signature->show();
	signature->sign(result);


	std::cout << "Done encoding. Spent time: " << timer.stop() << "s\n";


	return result;
}

std::string Magenta::decode(std::string C) {
	std::string result = "";

	Timer timer;
	timer.start();
	std::cout << "Starting decoding...\n";

	signature->init(C, "decoding");
	//signature->show();

	uint128_t previous_P = toUint128(C.substr(C.size() - 16, 16));
	C.erase(C.size() - 16, 16);

	for (size_t i = 0; i < C.length() / 16; i++) {
		std::string str = C.substr(16*i, 16);

		uint128_t C = toUint128(str);
		uint128_t P = decode(C ^ previous_P);
		previous_P = P;

		str = toString(P);
		result += str;
	}

	if (C.length() % 16 != 0)
		result = "error";

	while (*result.rbegin() == char(0))
		result.pop_back();
	result.pop_back();

	std::cout << "Signature check: ";
	if (!signature->checkSignature(result)) {
		std::cout << "wrong signature!\n";
		system("pause");
	}
	else
		std::cout << "OK\n";


	std::cout << "Done decoding. Spent time: " << timer.stop() << "s\n";


	return result;
}

void Magenta::encode(std::ifstream &input, std::ofstream &output, std::string additional_signature_info) {
	std::string M = "";

	char c;
	while (input.get(c))
		M += c;

	std::string C = encode(M, additional_signature_info);
	for (size_t i = 0; i < C.size(); i++) {
		int a = int(unsigned char(C[i])) / 16,
			b = int(unsigned char(C[i])) % 16;
		output << hex[a] << hex[b];
	}
}

void Magenta::decode(std::ifstream &input, std::ofstream &output) {
	std::string C = "";

	char a, b;
	while (input.get(a) && input.get(b))
		C += char(rev_hex[a]*16 + rev_hex[b]);

	std::string M = decode(C);
	for (size_t i = 0; i < M.size(); i++)
		output << M[i];
}