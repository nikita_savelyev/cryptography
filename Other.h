#ifndef OTHER_H
#define OTHER_H

#include <boost/multiprecision/cpp_int.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <string>
#include <stack>
#include <map>

using namespace boost::multiprecision;


struct Timer {
	std::stack <time_t> time_stack;
	Timer();
	void start();
	double stop();
};


int getByte(uint128_t arg, int pos);

int getByte(uint256_t arg, int pos);

uint128_t getByteSegment(uint128_t arg, int left, int right);

uint128_t connect8ByteSegments(uint128_t arg1, uint128_t arg2);

uint128_t toUint128(std::string arg);

uint256_t toUint256(std::string arg);

std::string toString(uint128_t arg);

std::string toString(uint256_t arg);

uint256_t getRandomValue(uint256_t left_boundary = 0, uint256_t right_boundary = -1);

uint256_t GCD(uint256_t arg1, uint256_t arg2);

uint256_t pow(uint256_t arg1, uint256_t arg2, uint256_t mod);

bool isPrime(uint256_t n, int number_of_tests = 100);

uint128_t getBigPrime();

uint128_t getMaxFactor(uint128_t n);

uint256_t getBiggerPrime(uint128_t q);

void printPairOfPrimes();


#endif