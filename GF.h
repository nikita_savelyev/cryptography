#ifndef GF_H
#define GF_H
#include "Includes.h"
using namespace std;


#define POLYNOM_POWER 9
#define EPS 1e-8


class Polynom {
public:
	double coefficients[POLYNOM_POWER];

	Polynom(double _coefficients[POLYNOM_POWER]);
	Polynom(const double _coefficients[POLYNOM_POWER]);
	Polynom();

	int getMaxPower() const;
	void show();

	Polynom operator = (const Polynom& other);
	
};

Polynom operator + (const Polynom& first, const Polynom& second);

Polynom operator - (const Polynom& first, const Polynom& second);

Polynom operator * (const Polynom& first, const Polynom& second);

Polynom operator % (const Polynom& first, const Polynom& second);

Polynom operator - (const Polynom& other);

int getFieldElem(int x);

const double default_polynom_coefficiets[POLYNOM_POWER] = {1, 0, 1, 0, 0, 1, 1, 0, 1 };
const Polynom defualt_polynom(default_polynom_coefficiets);

const double generating_polynom_coefficients[POLYNOM_POWER] = { 0, 1, 0, 0, 0, 0, 0, 0, 0 };
const Polynom generating_polynom(generating_polynom_coefficients);


#endif